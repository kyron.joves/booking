<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Redirect;
use App\Book;
use App\User;

class BookRequestController extends Controller
{
    function index($id){
    	$book = Book::find($id);
    	   	

    	return view('books.borrow_book', compact('book'));
    }

    function create($id, Request $request){
    	$book = Book::find($id);
        $user = Auth()->user(); 
        $quantity = $request->borrowQuantity;
        

        if ($user->books->find($id)) {
            if ($user->books->find($id)->pivot->status == 0) {
                $quantity += $user->books->find($id)->pivot->quantity;
                $user->books()->updateExistingPivot($id, ['quantity' => $quantity]);
                Session::flash('message',"Please wait for the administrator to approve your request");
                return Redirect::back();
                
            } else {
                $user->books()->attach($id, ['quantity'=> $quantity]);
                Session::flash('message',"Please wait for the administrator to approve your request");
                return Redirect::back();
            }
      
        }else {
        $user->books()->attach($id, ['quantity'=> $quantity]);
        Session::flash('message',"Please wait for the administrator to approve your request");
        return Redirect::back();
        }

    }
    function view(){
        $user = Auth()->user(); 
        $lists= $user->books()->get();
        $users = User::all();
        
        // dd($lists);
        

       return view('books.borrow_list', compact('user', 'lists', 'users'));

    }
    function approve($id, Request $request){
        $book = Book::find($id); 
        $user = User::find($request->userId);
        $bookQuantity = Book::find($id)->stock;
        $requestQuantity = $request->requestQuantity;
        if ($bookQuantity >= $requestQuantity) {
            $bookQuantity -= $requestQuantity;
            $book->stock = $bookQuantity;
            $book->save();
            $user->books()->wherePivot('id', $request->orderId)->updateExistingPivot($id, ['status' => 1]);
        
            Session::flash('message',"Request Approved");
            return Redirect::back();
        }
        else {
            Session::flash('error',"Not enough stocks, cannot approve");
            return Redirect::back();
        }
        
    }
    function decline($id, Request $request){
         $user = User::find($request->userId);
         $comment = $request->comment;
         
         $user->books()->wherePivot('id', $request->orderId)->updateExistingPivot($id, ['status' => 2, 'comment' => $comment]);
         
         Session::flash('error',"Request Declined");
         return Redirect::back();

    }
    function return($id, Request $request){
        $pivotId = $request->borrowId;
        $user = User::find($request->userId);
        $book = Book::find($id);  
        $requestQuantity = $request->requestQuantity;
        $requestQuantity -= $request->returnQuantity; 

        if ($requestQuantity == 0) {
          $book->stock += $request->returnQuantity;
          $user->books()->wherePivot('id', $request->borrowId)->updateExistingPivot($id, ['status' => 3]);
          $book->save(); 
          Session::flash('message',"Transaction Complete");
          return Redirect::back(); 
        }
        else{
        $book->stock += $request->returnQuantity;
        $book->save();
        $user->books()->wherePivot('id', $pivotId)->updateExistingPivot($id, ['quantity' => $requestQuantity]);
        Session::flash('message',"Quantity updated");
        return Redirect::back();
        }
    }
}

