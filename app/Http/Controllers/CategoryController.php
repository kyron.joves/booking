<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Session;
use Redirect;
class CategoryController extends Controller
{
    function index(){
    	$categories= Category::all();

    	return view('categories.view_categories', compact('categories'));
    }
    function create(Request $request){
    	$new_category = new Category();
    	$new_category->name = $request->name;
    	$new_category->save();

		Session::flash('message',"Category added successfully");
		return redirect('/categories');
    }
    function edit($id, Request $request){
    	$categories= Category::all();
    	$category = Category::find($id);
    	
    	$category->name =$request->name;
    	$category->save();

    	Session::flash('message',"Category edited successfully, please refresh to see the changes");
    	return Redirect::back();
    }

    function delete($id){
			// dd($id);
		$category = Category::find($id);
		$category->delete();
		
		Session::flash('message', "Item deleted successfully");
		return redirect('/categories');
	}

}
