<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Book;
use App\Category;
use Session;
use Validator;
class BookController extends Controller
{
	
    function index(Request $request){
	 $search= $request->get('search');
	 $categories= Category::all();
	

	// dd($result);  
	if ($request->has('search')) {
				
		$books = Book::where('name', 'like', '%' .$search. '%')->get();

    }
    else{
    $books = Book::all();
    }

    return view ('books.book_list', compact('books', 'categories'));
}
	function create(){
		$categories= Category::all();

		return view ("books.create_book", compact('categories'));
	}

	function view($id){
		$book = Book::find($id);
		$categories= Category::all(); 
		return view ('books.view_book', compact('book', 'categories'));
	}

	function edit($id){
		$book = Book::find($id);
		$categories= Category::all();

		return redirect ('books.edit_book', compact('book', 'categories'));

	}

	function update($id, Request $request){
		$categories= Category::all();
		$book = Book::find($id);
		$book->name = $request->name;
		$book->description = $request->description;
		$book->stock = $request->stock;
		$book->category_id = $request->category_id;
		$book->image = $request->image->move('../assets/images/', $request->file('image')->getClientOriginalName());
		
		$book->save();

		Session::flash('message', "Item updated successfully");
		
		return view('books.view_book', compact('book', 'categories'));
		
	}
	function save(Request $request){
		

		$validatedData = Validator::make($request->validate([
       'isbn' => 'required|min:10|max:13',
       'category_id' => 'required',
       'description' => 'required',
       'stock' => 'required',
       'image' => 'required',
       'name' => 'required',
  		 ]));



		$request->image->move('../assets/images/', $request->file('image')->getClientOriginalName());

		$new_book = new Book();
		$new_book->name = $request->name;
		$new_book->description = $request->description;
		$new_book->isbn = $request->isbn;
		$new_book->stock = $request->stock;
		$new_book->category_id = $request->category_id;
		$new_book->image = '../assets/images/'.$request->image->getClientOriginalName();

		
		$new_book->save();
		Session::flash('message',"Book added successfully");
		return redirect('/books');


	}
	
		function delete($id){
			// dd($id);
		$book = Book::find($id);
		$book->delete();
		
		Session::flash('message', "Item deleted successfully");
		return redirect('/books');
	}
	
	
}	
