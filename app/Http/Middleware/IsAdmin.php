<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Redirect;
use Session;
class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::user() && Auth::user()->role_id==1) {
            return $next($request);
        }
        Session::flash('message', 'You do not have administrative rights to perform such action');
        return Redirect::back();
    }
}
