<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    function user(){
    	return $this->belongsTo('App\User'); /*this is Orders*/
    }
    function status(){
    	return $this->belongsTo('App\Status');
    }
    function items(){
    	return $this->belongsToMany('App\Item', 'order_details')->withPivot('quantity');
    }
}
