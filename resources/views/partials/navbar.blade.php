<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="nav nav-item nav-link" href="/home">Library</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
    <div class="navbar-nav">


      @if(Auth::user() && Auth::user()->role_id == 1)
        
        
        <a class="nav-item nav-link" href="/books"><button class="btn btn-light">Books</button></a>
        <a class="nav-item nav-link" href="/create"><button class="btn btn-light">Add Book</button></a>
        <a class="nav-item nav-link" href="/transactions"><button class="btn btn-light">Requests/Transactions</button></a>
        <a class="nav-item nav-link" href="/categories"><button class="btn btn-light">Categories</button></a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST">
          {{csrf_field()}}
          <a class="nav-item nav-link" href=""><button class="btn btn-light">Logout</button></a>
        </form>
        @elseif(Auth::user() && Auth::user()->role_id == 2)
        <a class="nav-item nav-link" href="/books">Books <span class="sr-only">(current)</span></a>
        <a class="nav-item nav-link" href="/transactions">Requests/Transactions</a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" >
          {{csrf_field()}}
          <button class="btn btn-light" href="">Logout</button>
        </form>
        @else
       @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                       
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
      @endif
      

      
    </div>
  </div>
</nav>