@extends('partials.template')
@section ('content')


<div class="search-box" style="background-color: #ABA88A; width: 100%; height: 40vh; text-align: center; padding: 150px; margin-bottom: 20px" >
	 
<form method="get" action="/books/{name}">
	{{ csrf_field() }}
	<span id="comment"></span>
<input type="text" class="form-control" placeholder="Search Book" aria-label="search-book" name="search" id="searchTxt" style="margin-bottom: 20px">

<button type="submit" class="btn btn-success" id="searchBtn">Search</button>
</form>
</div>
<strong style="margin-left: 20px">Book List</strong><hr>
<div class="row">
	
<div class="book-conatiner" style="padding: 0 20px 0 20px;">
	
	@foreach($books as $book)
		<div class="card col-lg-3 col-md-6" id="card {{$book->id}}" style="width: 40%; display: inline-block; box-sizing: content-box;">
			<img class="card-img-top" src="{{$book->image}}" alt="Card image cap">
			<div class="card-title">
				<strong>{{$book->name}}</strong>
			</div>
			<div class="card-body"data-id={{$book->id}}>
				Stock: {{$book->stock}}
				<div class="modal-footer">
				
				</form>
				@if(Auth::user() && Auth::user()->role_id == 2)
				<a href="/borrow/{{$book->id}}"><button class="btn btn-warning" style="width: 90px !important;">Borrow</button></a>
				@else
				<a href="/view/{{$book->id}}"><button type="button" class="btn btn-primary" id="{{$book->id}}">View</button></a>
				<form method="post" action="/delete/book/{{$book->id}}">
					{{ csrf_field() }}
					{{method_field('DELETE')}}
				<button type="button" class="btn btn-danger delBtn" id="{{$book->id}}" data-toggle="modal" data-target="#deleteBookModal">Delete</button>
				@endif
				
				</div>
			</div>

		</div>
	@endforeach
</div>
</div>
<div class="modal fade" id="deleteBookModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Delete Book</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="editForm" method="post" action="" name="deleteBookModal">
			{{csrf_field()}}
			{{method_field('DELETE')}}
			Are you sure?
			

			<div class="modal-footer">
	        	
	        	<button class="btn btn-primary" id="saveBtn">Save changes</button>
	        	<button type="button"class="btn btn-danger" data-dismiss="modal">Cancel</button>
	      </div>
		</form>
      </div>
      
    </div>
  </div>
</div>
<script type="text/javascript">
	document.addEventListener("click", function(e){
		if (e.target.classList.contains('delBtn')) {
			
			let id = e.target.parentElement.parentElement.parentElement.getAttribute('data-id');
			
			editForm.action = '/delete/book/'+id;
			id.value = id;
			console.log(id);
		}
	});

	searchTxt.addEventListener("input", function(){
		if (searchTxt.value == "") {
			searchBtn.disabled = true;
			comment.style.color = "red";
			comment.innerHTML = "No text in this field";
		}else {
			searchBtn.disabled = false;
			comment.innerHTML="";
		}
	});
</script>
@endsection

