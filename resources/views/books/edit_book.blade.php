@extends('partials.template')
@section('content')
<form method="post" action="/edit/{{$book->id}}">
			{{csrf_field()}}
			{{method_field('PUT')}}
			Name: <input type="text" name="name" class="form-control" value="{{$book->name}}"></input>
			Description: <input type="text" name="description" class="form-control" value="{{$book->description}}"></input>
			Stock: <input type="number" name="price" class="form-control" value="{{$book->stock}}"></input>
			Category:<br>
			
			<select name="category_id">
				@foreach($categories as $category)
					@if($book->category_id == $category->id)
						<option selected value="{{ $category->id }}" >{{ $category->name }}</option>
					@else
					<option value="{{ $category->id }}" >{{ $category->name }}</option>	
					@endif
				@endforeach
			</select><br>
			<div class="modal-footer">
	        	<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	        	<button type="button" class="btn btn-primary" data-dismiss="modal">Save changes</button>
	      </div>
		</form>
@endsection