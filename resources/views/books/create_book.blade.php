@extends('partials.template')
@section ('content')

<div style="width: 80%; margin: 30px auto 85px auto; background-color: #ABA88A; padding: 50px; border-radius: 10px">
	<form method="post" action="/save_book" enctype="multipart/form-data">
		{{csrf_field()}}

		  <div class="form-group">
		    <label for="name">Book Name: </label>
		    <input type="text" class="form-control" id="name" name="name">
		    
		  </div>
		  <div class="form-group">
		    <label for="isbn">ISBN: </label>
		    <input type="text" class="form-control" id="isbn" name="isbn">
		  </div>
		  <div class="form-group">
		    <label for="description">Description</label>
		    <textarea type="number" class="form-control" id="description" name="description" rows="5" cols="50"></textarea>
		  </div>
		  <div class="form-group">
		    <label for="stock">Stock</label>
		    <input type="number" class="form-control" id="stock" name="stock">
		  </div>
		  <div class="form-group">
		    <label for="category">Category</label>
		    <select name="category_id" id="category">
					@foreach($categories as $category)
						
						<option value="{{ $category->id }}" >{{ $category->name }}</option>	
						
					@endforeach
				</select>
		  </div>
		  <div class="form-group">
		    <label for="image">Upload Image: </label>
		    <input type="file" class="form-control" id="image" name="image">
		  </div>
	  <button type="submit" class="btn btn-primary">Submit</button>
	</form>
</div>

<script type="text/javascript">
	

</script>
@endsection