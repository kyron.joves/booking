@extends ('partials.template')

@section('content')

	<div class="row">
		{{-- @if(Auth::user()->role_id == 2)
			User
			@else
			Admin
		@endif --}}
		<div class="container">
			<h4>Lists</h4>
			<table class="table">

			  <thead>
			    <tr>
			      <th>Book ID</th>
			      <th>Book Name</th>
			      <th>Quantity</th>
			      <th>Status</th>
			    @if(Auth::user()->role_id == 2)  
			    	 <th>Comment</th>
			     @endif 
			  
			  	@if(Auth::user()->role_id == 1)
			  	<th>Options</th>
			      
			    </thead>
			  <tbody>
			  		@foreach($users as $user)
			  			@foreach($user->books as $book)
			  			<tr>
			  				<td>{{$user->name}}</td>
			  				<td>{{$book->name}}</td>
			  				<td data-id="{{$book->pivot->quantity}}">{{$book->pivot->quantity}}</td>
			  				@if($book->pivot->status == 0)
						      <td>Pending</td>
						      <form method="post" action="transactions/{{$book->id}}/approve" data-id="{{$book->id}}">
						      	{{csrf_field()}}
						      	{{method_field('PUT')}}
						      <td>{{-- {{$book->pivot->id}} --}}
						      	<input type="number" id="quantity" name="requestQuantity" value="{{$book->pivot->quantity}}" hidden="">
						      	<input type="name" name="userId" value="{{$user->id}}"hidden>
						      	<input type="name" name="orderId" value="{{$book->pivot->id}}"hidden>
						      	<button class="btn btn-success" style="text-align: center;">Approve</button>
						      </form>
						      {{-- <form method="post" action="transactions/{{$book->id}}/decline" data-id="{{$book->id}}" style=" display: inline-block;">
						      	{{csrf_field()}}
						      	{{method_field('PUT')}} --}}
						      	<input type="number" id="quantity" name="requestQuantity" value="{{$book->pivot->quantity}}" hidden="">
						      	<input type="name" name="userId" value="{{$user->id}}"hidden>
						      	<input type="name" name="orderId" value="{{$book->pivot->id}}"hidden>
						      	<button class="btn btn-danger" style="text-align: center; margin-right: 10px;" data-toggle="modal" data-target="#declineRequestModal">Decline</button>
						      	{{-- </form> --}}
						      	
						      </td>
						     
						      @elseif($book->pivot->status == 1)
						      <td><strong>Approved</strong></td>
						      <td>
						      	@if($book->pivot->quantity != 0)
						      	<form method="post" action="return/{{$book->id}}">
					      			{{csrf_field()}}
									{{method_field('PUT')}}
									<input type="number" id="quantity" name="requestQuantity" value="{{$book->pivot->quantity}}" hidden="">
									<input type="name" name="userId" value="{{$user->id}}"hidden>
									<input type="name" name="borrowId" value="{{$book->pivot->id}}" hidden>
						      		<input type="number" name="returnQuantity"  class="form-control retQuant" style="width: 20%; display: inline !important"></input>
						      		<button class="btn btn-outline-primary" style="margin-left:20px" id="returnBtn">Return</button>
						      		<span id="comment" style="font-size: 15px"></span>
						      	</form>
						      	@endif
						      </td>
						      @elseif($book->pivot->status == 2)
						      
							      <td>
							      	<strong>Declined</strong>
							      </td>
							      <td></td>
							   @elseif($book->pivot->status == 3)
							   
							      <td>
							      	<strong>Completed</strong>
							      </td>
							      <td></td>
						      @endif
						      
						</tr>
						<div class="modal fade" id="declineRequestModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Decline Request Comment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="transactions/{{$book->id}}/decline" data-id="{{$book->id}}" style=" display: inline-block;">
      	{{csrf_field()}}
      	{{method_field('PUT')}}
      	<input type="number" id="quantity" name="requestQuantity" value="{{$book->pivot->quantity}}" hidden="">
      	<input type="text" name="userId" value="{{$user->id}}" id="userIdInp" hidden>
      	<input type="text" name="orderId" value="{{$book->pivot->id}}" id="orderIdInp" hidden> 	
        <textarea class="form-control" rows="5" cols="50" name="comment">
        	
        </textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button class="btn btn-primary">Save changes</button>
      </div>
  	</form>
    </div>
  </div>
</div>
			  			@endforeach
			  		@endforeach
			  	@endif
			  	

			  	
			      
			    </tr>
			  	</thead>
			  	@foreach($lists as $list)
			    <tr>
			      <td>{{$list->id}}</td>
			      <td>{{$list->name}}</td>
			      <td>{{$list->pivot->quantity}}</td>
			      @if($list->pivot->status == 0)
			      <td>Pending</td>
			      <td></td>
			      @elseif ($list->pivot->status == 1)
			      <td>Approved</td>
			      <td></td>
			      @elseif($list->pivot->status == 2)
			      <td>Declined</td>
			      <td>{{$list->pivot->comment}}</td>
			      @else
			      <td>Completed</td>
			      <td></td>
			      @endif
			    </tr>
			    @endforeach
			    </tbody>

			</table>

		</div>
	</div>



<!-- Modal -->




	<script type="text/javascript">
		 
		let retQuants = document.querySelectorAll('.retQuant');

		retQuants.forEach( function(retQuant){
			retQuant.addEventListener("input", function(){
				let quantity = retQuant.parentElement.parentElement.parentElement.childNodes[5].getAttribute('data-id');
				let button = retQuant.nextElementSibling;
				let comment = retQuant.nextElementSibling.nextElementSibling;
				if (retQuant.value > quantity) {
					button.disabled = true;
					comment.style.color = "red";
					comment.innerHTML = "Input Invalid"
				}else{
					comment.innerHTML = ""
					button.disabled = false;
				}
			});
		});

		let declineBtns = document.querySelectorAll('.btn-danger');
		declineBtns.forEach( function (declineBtn){
			declineBtn.addEventListener("click", function(){
				let orderId = declineBtn.previousElementSibling.getAttribute('value');
				let userId = declineBtn.previousElementSibling.previousElementSibling.getAttribute('value');
				console.log(userId, orderId);

				userIdInp.value = userId;
				orderIdInp.value = orderId;
			});
		});
		

	</script>
@endsection