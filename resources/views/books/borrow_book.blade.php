@extends('partials.template')

@section('content')

	<div class="row">
		<div class="container" style="padding: 50px 20px 50px 20px; border: 1px solid black; border-radius: 10px">
		<div style="width: 100%">
			<form method="post" action="/borrow/{{$book->id}}/create"  data-id="{{$book->stock}}">
			{{csrf_field()}}
			Name: <p>{{$book->name}}</p>
			<hr>
			Current Quantity: <p id="stock" value="{{$book->stock}}">{{$book->stock}}</p>
			<hr>
			How many do you wish to borrow? <input type="text" name="borrowQuantity" id="borrowInput" style="border-radius: 5px">
			<span id="comment" style="font-size: 15px"></span>
			<hr>
			<button class="btn btn-success" id="submitBtn" disabled>Submit</button>
			</form>
		</div>
		</div>
	</div>
<script type="text/javascript">
	
		stock = stock.getAttribute("value");
		console.log(stock);
		borrowInput.addEventListener("input", function(){
			if (parseInt(stock) >= borrowInput.value) {
				comment.innerHTML = "";
				submitBtn.disabled = false;
			}else{
				comment.innerHTML = "Invalid input";
				submitBtn.disabled = true;
			}

		});

</script>
@endsection