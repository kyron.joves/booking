@extends ('partials.template')

@section('content')

<div class="card" style="width: 50%;margin: 50px auto 50px auto; background-color: #D8D7C9;">
  <div class="card-header">
    {{$book->name}}
  </div>
<form style=" padding: 20px">
	
		<strong>ISBN: </strong>{{$book->isbn}}<hr>
		<strong>Description: </strong>{{$book->description}}<hr>
		<strong>Stock: </strong>{{$book->stock}}<hr>
		
		<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#editModal">Edit</button>
		
</form>
</div>
<!-- Button trigger modal -->


<!--Edit Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/edit/{{$book->id}}" enctype="multipart/form-data">
			{{csrf_field()}}
			{{method_field('PUT')}}
			Name: <input type="text" name="name" class="form-control" value="{{$book->name}}"></input>
			Description: <input type="text" name="description" class="form-control" value="{{$book->description}}"></input>
			Stock: <input type="number" name="stock" class="form-control" value="{{$book->stock}}"></input>
			
			Image: <input type="file" name="image" class="form-control"><hr>
			Category:<br>
			<select name="category_id">
				@foreach($categories as $category)
					@if($book->category_id == $category->id)
						<option selected value="{{ $category->id }}" >{{ $category->name }}</option>
					@else
					<option value="{{ $category->id }}" >{{ $category->name }}</option>	
					@endif
				@endforeach
			</select><br>

			<div class="modal-footer">
	        	
	        	<button class="btn btn-primary" id="saveBtn">Save changes</button>
	        	<button type="button"class="btn btn-danger" data-dismiss="modal">Cancel</button>
	      </div>
		</form>
      </div>
      
    </div>
  </div>
</div>

@endsection