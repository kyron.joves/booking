@extends ('partials.template')

@section('content')

<div class="row">
	<div style="height: 200px; padding-top: 80px; background-color: #ABA88A; width: 100%; text-align: center; padding: 70px 40px 0 40px">
		<form method="post" action="/categories">
			{{csrf_field()}}
			<input class="form-control" type="text" placeholder="Add category here" name="name" id="addText" style="margin-bottom: 20px"></input>
			<button class="btn btn-success" id="submit">Submit</button>
		</form>
	</div>	
</div>
<div class="row">
	<div style="margin: 0 auto; width: 100%">
		<table class="table table-striped"> 
		  <thead>
		    <tr>
		      <th>ID</th>
		      <th>Name</th>
		      <th></th>
		      <th></th>
		    </tr>
		  </thead>
		  <tbody>
		@foreach($categories as $category)
			<tr id="catTable" data-id="{{$category->id}}" data-name="{{$category->name}}">
			 
		      <td>{{$category->id}}</td>
		      <td>{{$category->name}}</td>
		      <td><button class="btn btn-info editCatBtn" data-toggle="modal" data-target="#editCat" data-id="{{$category->id}}">Edit</button></td>
		      <td>
		      	<form method="post" action="/delete/{{$category->id}}">
					{{ csrf_field() }}
					{{method_field('DELETE')}}
				<button type="submit" class="btn btn-danger" id="{{$category->id}}">Delete</button>
				</form>
			</td>
		  	</th>
		  	<tr>
		@endforeach
		</tbody>
	</table>
	</div>
</div>

<div class="modal fade" id="editCat" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="/categories/{{$category->id}}" name="editForm">
			{{csrf_field()}}
			{{method_field('PUT')}}
			
			Name: <input type="text" name="name" class="form-control" id="editInp"></input>

			<div class="modal-footer">
	        	
	        	<button class="btn btn-primary" id="saveBtn">Save changes</button>
	        	<button type="button"class="btn btn-danger" data-dismiss="modal">Cancel</button>
	      </div>
		</form>
      </div>
      
    </div>
  </div>
</div>

<script type="text/javascript">
	document.addEventListener("click", function(e){
		if (e.target.classList.contains('editCatBtn')) {
			let name =  e.target.parentElement.parentElement.getAttribute('data-name');
			let id = e.target.parentElement.parentElement.getAttribute('data-id');
			document.editForm.action = /categories/+id;
			editInp.value = name;
			editId.value = id;
			// console.log(name,id);
		}
	});
	let input = document.querySelector('#addText');
	let submitBtn = document.querySelector('#submit')
	input.addEventListener("click", function(){
		if (input.value == "") {
			submitBtn.disabled = true;
		}else {
			submitBtn.disabled = false;
		}
	});
</script>
@endsection

