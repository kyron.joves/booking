<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view ("welcome");
// });


Route::get('/', function () {
    return view ("welcome");
});
Route::get('/books','BookController@index');
Route::get('books/{name}','BookController@index');

Route::get('/create', 'BookController@create');
Route::post('/save_book', 'BookController@save');

Route::get('view/{id}','BookController@view');
Route::get('edit/{id}','BookController@edit');
Route::put('/edit/{id}','BookController@update');
Route::delete('/delete/book/{id}','BookController@delete');

/*categories*/
Route::get('/categories','CategoryController@index');
Route::post('/categories', 'CategoryController@create');
Route::put('/categories/{id}', 'CategoryController@edit');
Route::delete('/delete/{id}','CategoryController@delete');

Route::get('/borrow/{id}', 'BookRequestController@index');
Route::post('/borrow/{id}/create', 'BookRequestController@create');
Route::get('/transactions', 'BookRequestController@view');
Route::put('/transactions/{id}/approve', 'BookRequestController@approve');
Route::put('/transactions/{id}/decline', 'BookRequestController@decline');
Route::put('/return/{id}', 'BookRequestController@return');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

